import {useState, useEffect} from "react";

const User = () => {
    
    const [isLogged, setIsLogged] = useState(false);

    useEffect(() => {
        const isTokenSet = localStorage.getItem("demo-token");
        setIsLogged(isTokenSet);
        document.title = isTokenSet ? "Welcome User" : "Please Login";
    }, [isLogged])

    const handleLogin = () => {
        localStorage.setItem("demo-token", true);
        setIsLogged(true);
    } 
    
    const handleLogout = () => {
        localStorage.removeItem("demo-token");
        setIsLogged(false);
    } 

    return (
        <div> 
            <h1>{isLogged ? "Welcome user!" : "Please login"}</h1>
            {
                isLogged ? 
                <button onClick={handleLogout}>Log out</button>
                :
                <button onClick={handleLogin}>Login</button>
            }    
        </div>
    )
}

export default User